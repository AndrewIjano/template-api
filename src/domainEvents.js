module.exports = {
  userCreatedEvent: (user) =>
    JSON.stringify({
      eventType: "UserCreated",
      entityId: user.id,
      entityAggregate: {
        name: user.name,
        email: user.email,
        password: user.password,
      },
    }),

  userDeletedEvent: (user) =>
    JSON.stringify({
      eventType: "UserDeleted",
      entityId: user.id,
      entityAggregate: {},
    }),
};
