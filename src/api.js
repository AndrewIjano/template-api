const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

const {
  MissingFieldError,
  MalformedFieldError,
  PasswordNotMatchError,
  AccessTokenNotFoundError,
  InvalidAccessTokenError,
  AccessTokenNotMatchError,
  UserNotFoundError,
} = require("./errors");
const { userCreatedEvent, userDeletedEvent } = require("./domainEvents");

const emailType = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
const alphanumType = /^[0-9a-zA-Z]+$/;

const getTokenPayload = (headers, secret) => {
  if (!headers.authorization) {
    throw new AccessTokenNotFoundError();
  }

  if (
    headers.authorization.split(" ").length !== 2 ||
    headers.authorization.split(" ")[0] !== "Bearer"
  ) {
    throw new AccessTokenNotFoundError();
  }

  const [, token] = headers.authorization.split(" ");

  try {
    const payload = jwt.verify(token, secret);
    return payload;
  } catch (error) {
    throw new InvalidAccessTokenError(error.message);
  }
};

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();
  const db = mongoClient.db("database");

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  api.post("/users", async (req, res) => {
    try {
      const { name, email, password, passwordConfirmation } = req.body;
      const users = db.collection("users");

      const requiredFields = [
        "name",
        "email",
        "password",
        "passwordConfirmation",
      ];

      requiredFields.forEach((fieldName) => {
        if (req.body[fieldName] === undefined) {
          throw new MissingFieldError(fieldName);
        }
      });

      if (name === "") {
        throw new MalformedFieldError("name");
      }

      if (!emailType.test(email) || (await users.findOne({ email }))) {
        throw new MalformedFieldError("email");
      }

      if (
        !alphanumType.test(password) ||
        password.length < 8 ||
        password.length > 32
      ) {
        throw new MalformedFieldError("password");
      }

      if (
        !alphanumType.test(passwordConfirmation) ||
        passwordConfirmation.length < 8 ||
        passwordConfirmation.length > 32
      ) {
        throw new MalformedFieldError("passwordConfirmation");
      }

      if (password != passwordConfirmation) {
        throw new PasswordNotMatchError();
      }

      const user = { id: uuid(), name, email, password };

      stanConn.publish("users", userCreatedEvent(user));

      res.status(201).json({
        user: {
          id: user.id,
          name: user.name,
          email: user.email,
        },
      });
    } catch (error) {
      const status = (() => {
        switch (error.constructor) {
          case MissingFieldError:
          case MalformedFieldError:
            return 400;
          case PasswordNotMatchError:
            return 422;
          default:
            return 500;
        }
      })();

      res.status(status).json({ error: error.message });
    }
  });

  api.delete("/users/:uuid", async (req, res) => {
    try {
      const { uuid } = req.params;
      const users = db.collection("users");

      const tokenPayload = getTokenPayload(req.headers, secret);

      if (tokenPayload.id !== uuid) {
        throw new AccessTokenNotMatchError();
      }

      const user = await users.findOne({ id: uuid });
      if (!user) {
        throw new UserNotFoundError();
      }

      stanConn.publish("users", userDeletedEvent(user));

      res.status(200).json({ id: uuid });
    } catch (error) {
      const status = (() => {
        switch (error.constructor) {
          case AccessTokenNotFoundError:
          case InvalidAccessTokenError:
            return 401;
          case AccessTokenNotMatchError:
            return 403;
          case UserNotFoundError:
            return 404;
          default:
            return 500;
        }
      })();

      res.status(status).json({ error: error.message });
    }
  });

  return api;
};
