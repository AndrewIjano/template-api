class MissingFieldError extends Error {
  constructor(fieldName) {
    const message = `Request body had missing field ${fieldName}`;
    super(message);
    this.fieldName = fieldName;
  }
}

class MalformedFieldError extends Error {
  constructor(fieldName) {
    const message = `Request body had malformed field ${fieldName}`;
    super(message);
    this.fieldName = fieldName;
  }
}

class PasswordNotMatchError extends Error {
  constructor() {
    const message = `Password confirmation did not match`;
    super(message);
  }
}

class AccessTokenNotFoundError extends Error {
  constructor() {
    const message = `Access Token not found`;
    super(message);
  }
}

class AccessTokenNotMatchError extends Error {
  constructor() {
    const message = `Access Token did not match User ID`;
    super(message);
  }
}

class InvalidAccessTokenError extends Error {
    constructor(errorMessage) {
      const message = `Invalid access token: ${errorMessage}`;
      super(message);
    }
  }

class UserNotFoundError extends Error {
  constructor() {
    const message = `User not found`;
    super(message);
  }
}

module.exports = {
  MissingFieldError,
  MalformedFieldError,
  PasswordNotMatchError,
  AccessTokenNotFoundError,
  InvalidAccessTokenError,
  AccessTokenNotMatchError,
  UserNotFoundError,
};
